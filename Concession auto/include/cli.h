#ifndef CLI_H
#define CLI_H
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;


class cli
{
    public:
        cli(string p_cliId)
        {
            ifstream monFlux;
            monFlux.open("C:\\cplusplus-balz\\Concession auto\\csv\\cli.csv");
            if(monFlux){
                string ligne;
                // on excepte la premi�re ligne du ficheir csv
                int i = 0;
                while(getline(monFlux,ligne))
                {
                    if (i!= 0){
                        istringstream f (ligne);
                        vector<string> chaines;
                        string s;
                        while (getline(f,s,','))
                        {
                            chaines.push_back(s);

                        }
                        if (chaines[0] == p_cliId){
                            cliId = std::stoi(chaines[0]);
                            nom = chaines[1];
                            prenom = chaines[2];
                        }
                    }
                    i = i+1;
                }
            }else
            {
                cout << "impossible d'ouvrir le fichier ";
            }
        }
        cli(string p_nom, string p_prenom)
        {
            nom = p_nom;
            prenom = p_prenom;
            cliId = getNewId();
        };
        virtual ~cli();

        int GetCliId()
        {
            return cliId;
        }
        void SetCliId(int val) { cliId = val; }
        string GetNom()
        {
            return nom;
        }
        void SetNom(string val) { nom = val; }
        string GetPrenom() { return prenom; }
        void SetPrenom(string val) { prenom = val; }

        void insertCsv(){
            string const fichier_bloc("C:\\cplusplus-balz\\Concession auto\\csv\\cli.csv");
            ofstream notes;
            notes.open(fichier_bloc, std::ios_base::app);
            notes << cliId << "," << nom<< "," << prenom<< endl;
            notes.close();
        }
        int getNewId(){
            ifstream monFlux;
            monFlux.open("C:\\cplusplus-balz\\Concession auto\\csv\\cli.csv");
            int newId = 0;
            if(monFlux)
            {
                string ligne;
                // on excepte la premi�re ligne du ficheir csv
                int i = 0;
                while(getline(monFlux,ligne))
                {
                    if ( i!= 0){
                        istringstream f (ligne);
                        vector<string> chaines;
                        string s;
                        while (getline(f,s,','))
                        {
                            chaines.push_back(s);

                        }
                        int l_venId = std::stoi(chaines[0]);
                        if ( l_venId > newId){
                            newId = l_venId;
                        }
                    }
                    i = i+1;
                }
            }else
            {
                cout << "impossible d'ouvrir le fichier ";
            }
            newId = newId +1;
            return newId;
        }

    protected:

    private:
        int cliId;
        string nom;
        string prenom;
};

#endif // CLI_H
