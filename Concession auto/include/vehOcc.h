#ifndef VEHOCC_H
#define VEHOCC_H
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <stdlib.h>

using namespace std;

class vehOcc
{
    public:
        virtual ~vehOcc();
        vehOcc(string vehId)
        {
            ifstream monFlux;
            monFlux.open("C:\\cplusplus-balz\\Concession auto\\csv\\vehOcc.csv");
            if(monFlux){
                string ligne;
                // on excepte la premi�re ligne du ficheir csv
                int i = 0;
                while(getline(monFlux,ligne))
                {
                    if (i!= 0){
                        istringstream f (ligne);
                        vector<string> chaines;
                        string s;
                        while (getline(f,s,','))
                        {
                            chaines.push_back(s);

                        }
                        if (chaines[0] == vehId){
                            immId = chaines[0];
                            marque = chaines[1];
                            modele = chaines[2];
                            typeEssence = chaines[3];
                            typeTransmission = chaines[4];
                            couleur = chaines[5];
                            nbPortes =  std::stoi(chaines[6]);
                            prix = std::stoi(chaines[7]);
                            nbKm = std::stoi(chaines[8]);
                        }
                    }
                    i = i+1;
                }
            }else
            {
                cout << "impossible d'ouvrir le fichier ";
            }
        }
        vehOcc(string p_immId,string p_marque,string p_modele,string p_typEssence,string p_typeTransmission,string p_couleur,string p_nbPortes,string p_prix,string p_nbKm){
            immId = p_immId;
            marque = p_marque;
            modele = p_modele;
            typeEssence = p_typEssence;
            typeTransmission = p_typeTransmission;
            couleur = p_couleur;
            nbPortes =  std::stoi(p_nbPortes);
            prix = std::stoi(p_prix);
            nbKm = std::stoi(p_nbKm);
        };
        string GetimmId() { return immId; }
        void SetimmId(string val) { immId = val; }
        string Getmarque() { return marque; }
        void Setmarque(string val) { marque = val; }
        string Getmodele() { return modele; }
        void Setmodele(string val) { modele = val; }
        string GettypeEssence() { return typeEssence; }
        void SettypeEssence(string val) { typeEssence = val; }
        string GettypeTransmission() { return typeTransmission; }
        void SettypeTransmission(string val) { typeTransmission = val; }
        string Getcouleur() { return couleur; }
        void Setcouleur(string val) { couleur = val; }
        int GetnbPortes() { return nbPortes; }
        void SetnbPortes(int val) { nbPortes = val; }
        int Getprix() { return prix; }
        void Setprix(int val) { prix = val; }
        int GetNbKm() { return nbKm; }
        void SetNbKm(int val) { nbKm = val; }

        void insertCsv(){
            string const fichier_bloc("C:\\cplusplus-balz\\Concession auto\\csv\\vehOcc.csv");
            ofstream notes;
            notes.open(fichier_bloc, std::ios_base::app);
            notes << GetimmId() << "," << Getmarque() << "," << Getmodele() << "," << GettypeEssence() << "," << GettypeTransmission() << "," << Getcouleur() << "," << GetnbPortes() << "," << Getprix() << "," << GetNbKm() << endl;
            notes.close();
        }

    bool estVendu(){
        ifstream monFlux;
        monFlux.open("C:\\cplusplus-balz\\Concession auto\\csv\\vehVen.csv");
        bool estVendu = false;
        if(monFlux)
        {
            string ligne;
            // on excepte la premi�re ligne du ficheir csv
            int i = 0;
            while(getline(monFlux,ligne))
            {
                if ( i!= 0){
                    istringstream f (ligne);
                    vector<string> chaines;
                    string s;
                    while (getline(f,s,','))
                    {
                        chaines.push_back(s);

                    }
                    if (immId == chaines[2]){
                        estVendu = true;
                    }

                }
                i = i+1;
            }
        }else
        {
            cout << "impossible d'ouvrir le fichier ";
        }
        return estVendu;
    }

    protected:

    private:
        string immId;
        string marque;
        string modele;
        string typeEssence;
        string typeTransmission;
        string couleur;
        int nbPortes;
        int prix;
        int nbKm;

};
#endif // VEHOCC_H
