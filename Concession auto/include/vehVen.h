#ifndef VEHVEN_H
#define VEHVEN_H
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class vehVen
{
    public:
        vehVen(string p_cliId,string p_venId,string p_vehId){
            cliId = std::stoi(p_cliId);
            venId = std::stoi(p_venId);
            immId = p_vehId;
        };
        virtual ~vehVen();

        int GetcliId() { return cliId; }
        void SetcliId(int val) { cliId = val; }
        int GetvenId() { return venId; }
        void SetvenId(int val) { venId = val; }
        string GetimmId() { return immId; }
        void SetimmId(string val) { immId = val; }
        void insertCsv(){
            string const fichier_bloc("C:\\cplusplus-balz\\Concession auto\\csv\\vehVen.csv");
            ofstream notes;
            notes.open(fichier_bloc, std::ios_base::app);
            notes << GetcliId() << "," << GetvenId() << "," << GetimmId() << endl;
            notes.close();
        }

    protected:

    private:
        int cliId;
        int venId;
        string immId;
};

#endif // VEHVEN_H
