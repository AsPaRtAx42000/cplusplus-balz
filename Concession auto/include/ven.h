#ifndef VEN_H
#define VEN_H
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;
class ven
{
    private:
        int venId;
        std::string nom;
        std::string prenom;

    public:
        ven(string p_venId)
        {
            ifstream monFlux;
            monFlux.open("C:\\cplusplus-balz\\Concession auto\\csv\\ven.csv");
            if(monFlux){
                string ligne;
                // on excepte la premi�re ligne du ficheir csv
                int i = 0;
                while(getline(monFlux,ligne))
                {
                    if (i!= 0){
                        istringstream f (ligne);
                        vector<string> chaines;
                        string s;
                        while (getline(f,s,','))
                        {
                            chaines.push_back(s);

                        }
                        if (chaines[0] == p_venId){
                            venId = std::stoi(chaines[0]);
                            nom = chaines[1];
                            prenom = chaines[2];
                        }
                    }
                    i = i+1;
                }
            }else
            {
                cout << "impossible d'ouvrir le fichier ";
            }
        }
        ven(string p_nom,string p_prenom){
            nom = p_nom;
            prenom = p_prenom;
            venId = getNewId();
        };
        virtual ~ven();

        int GetVenId() { return venId; }
        void SetVenId(int val) { venId = val; }
        std::string GetNom() { return nom; }
        void SetNom(std::string val) { nom = val; }
        std::string GetPrenom() { return prenom; }
        void SetPrenom(std::string val) { prenom = val; }

        int getNewId(){
            ifstream monFlux;
            monFlux.open("C:\\cplusplus-balz\\Concession auto\\csv\\ven.csv");
            int newId = 0;
            if(monFlux)
            {
                string ligne;
                // on excepte la premi�re ligne du ficheir csv
                int i = 0;
                while(getline(monFlux,ligne))
                {
                    if ( i!= 0){
                        istringstream f (ligne);
                        vector<string> chaines;
                        string s;
                        while (getline(f,s,','))
                        {
                            chaines.push_back(s);

                        }
                        int l_venId = std::stoi(chaines[0]);
                        if ( l_venId > newId){
                            newId = l_venId;
                        }
                    }
                    i = i+1;
                }
            }else
            {
                cout << "impossible d'ouvrir le fichier ";
            }
            newId = newId +1;
            return newId;
        }

        void insertCsv(){
            string const fichier_bloc("C:\\cplusplus-balz\\Concession auto\\csv\\ven.csv");
            ofstream notes;
            notes.open(fichier_bloc, std::ios_base::app);
            notes << venId << "," << nom << "," << prenom << endl;
            notes.close();
        }

    protected:


};

#endif // VEN_H
