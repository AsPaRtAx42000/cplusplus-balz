#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include "vehOcc.h"
#include "vehVen.h"
#include "ven.h"
#include "cli.h"

using namespace std;

// on boucle sur toutes les lignes du fichier csv client afin de les afficher
void affichageClients(){
    string const fichier_bloc("C:\\cplusplus-balz\\Concession auto\\csv\\cli.csv");
    ifstream notes(fichier_bloc.c_str());
    int i = 0;
    if(notes){
        string lig;
        while(getline(notes,lig))
        {
            if ( i != 0 ){
                cout << lig << endl;
            }
            i = i+1;
        }
    }else
    {
        cout << "impossible d'ouvrir le fichier ";
    }
}

// on boucle sur toutes les lignes du fichiers v�hicules afin de les afficher
void affichageVehicules(){
    string const fichier_bloc("C:\\cplusplus-balz\\Concession auto\\csv\\vehOcc.csv");
    ifstream notes(fichier_bloc.c_str());
    int i = 0;
    if(notes){
        string lig;
        while(getline(notes,lig))
        {
            if ( i != 0 ){
                cout << lig << endl;
            }
            i = i+1;
        }
    }else
    {
        cout << "impossible d'ouvrir le fichier ";
    }
}

// on parcout toutes les lignes du fichier csv vendeur, afin d'afficher toutes les lignes
void affichageVendeurs()
{
    string const fichier_bloc("C:\\cplusplus-balz\\Concession auto\\csv\\ven.csv");
    ifstream notes(fichier_bloc.c_str());
    int i = 0;
    if(notes){
        string lig;
        while(getline(notes,lig))
        {
            if ( i != 0 ){
                cout << lig << endl;
            }
            i = i+1;
        }
    }else
    {
        cout << "impossible d'ouvrir le fichier ";
    }
}

// chaque ligne du fichier csv contient le clientId, le vendeurId et le vehiculeId
void affichageVehiculesVendus(){
    ifstream monFlux;
    monFlux.open("C:\\cplusplus-balz\\Concession auto\\csv\\vehVen.csv");
    if(monFlux)
    {
        string ligne;
        // on excepte la premi�re ligne du ficheir csv
        int i = 0;

        // on boucle sur chaque ligne
        while(getline(monFlux,ligne))
        {
            if ( i!= 0){
                istringstream f (ligne);
                vector<string> chaines;
                string s;
                while (getline(f,s,','))
                {
                    chaines.push_back(s);

                }

                // on cr�� les objets de chaque classe
                vehOcc vehOcc(chaines[2]);
                ven ven(chaines[1]);
                cli cli(chaines[0]);

                // affichage des infos voulus
                cout << vehOcc.GetimmId() << " " << vehOcc.Getmarque() << " " << vehOcc.Getmodele() << " : "<< endl;
                cout << "       vendu par : " << ven.GetPrenom() << " " << ven.GetNom() << endl;
                cout << "       vendu au client : " << cli.GetPrenom() << " " << cli.GetNom() << endl;
                cout << "" << endl;
            }
            i = i+1;
        }
    }else
    {
        cout << "impossible d'ouvrir le fichier ";
    }
}

// on parcourt les lignes du fichier csv v�hicule
// la m�thode "est vendu" renvoi un bouloo�n
// elle regarde pour chaque v�hicule si son ID est dans le fichier CSV venVen
void AffichageVehDispo(){
    ifstream monFlux;
    monFlux.open("C:\\cplusplus-balz\\Concession auto\\csv\\vehOcc.csv");
    if(monFlux)
    {
        string ligne;
        // on excepte la premi�re ligne du ficheir csv
        int i = 0;
        while(getline(monFlux,ligne))
        {
            if ( i!= 0){
                istringstream f (ligne);
                vector<string> chaines;
                string s;
                while (getline(f,s,','))
                {
                    chaines.push_back(s);

                }
                vehOcc vehOcc(chaines[0]);
                if (vehOcc.estVendu() == false ){
                    cout << vehOcc.GetimmId() << " " << vehOcc.Getmarque() << " " << vehOcc.Getmodele() << endl;
                }
            }
            i = i+1;
        }
    }else
    {
        cout << "impossible d'ouvrir le fichier ";
    }
}

// on renseigne tous les champs, cr�� l'objet client & insert dans le fichier csv vehOcc
void AjoutVehicule(){
    string marque;
    cout << " Entrez la marque : " << endl;
    cin>>marque;

    string modele;
    cout << " Entrez le mod�le : " << endl;
    cin>>modele;

    string immat;
    cout << " Entrez l'immatriculation : " << endl;
    cin>>immat;

    string energie;
    cout << " Entrez l'energie (Diesel ou essence : " << endl;
    cin>>energie;

    string transmi;
    cout << " Entrez la transmission (Automatique ou mecanique) : " << endl;
    cin>>transmi;

    string couleur;
    cout << " Entrez la couleur : " << endl;
    cin>>couleur;

    string nbPorte;
    cout << " Entrez le nombre de portes : " << endl;
    cin>>nbPorte;

    string annee;
    cout << " Entrez l'annee: " << endl;
    cin>>annee;

    string km;
    cout << " Entrez le nombre de Km: " << endl;
    cin>>km;

    string prixVente;
    cout << " Entrez le prix de vente : " << endl;
    cin>>prixVente;

    vehOcc vehOcc(immat,marque,modele,energie,transmi,couleur,nbPorte,prixVente,km);
    vehOcc.insertCsv();

    cout << " Ajout du vehicule reussi " << endl;
}

// on choisit le client, le vendeur et le v�hicule disponible
// on cr�� l'objet vehVen, et insert les donn�es dans le fichier csv vehVen
void AjoutVente(){
    cout << " Quel vehicule a ete vendu ? Merci de rentrer son immatriculation : " << endl;
    AffichageVehDispo();
    string vehId;
    cin >> vehId;

    cout << " Qui a vendu ce vehicule ? Merci de rentrer son numero : " << endl;
    affichageVendeurs();

    string vendeurId;
    cin >> vendeurId;

    cout << " Qui a achete ce vehicule ? Merci de rentrer son numero : " << endl;
    affichageClients();

    string cliId;
    cin >> cliId;

    vehVen vehVen(cliId,vendeurId,vehId);
    vehVen.insertCsv();

    cout << "Ajout reussi !" << endl;
}


// on renseigne tous les champs, cr�� l'objet client & insert dans le fichier csv vendeur
void AjoutVendeur(){
    cout << "Quel est le nom du vendeur ? " << endl;
    string nom;
    cin>>nom;

    cout << "Quel est le prenom du vendeur ? " << endl;
    string prenom;
    cin>>prenom;

    ven ven(nom,prenom);
    ven.insertCsv();

    cout << "ajout reussi ! " << endl;

}

// on renseigne tous les champs, cr�� l'objet client & insert dans le fichier csv client
void AjoutClient(){
    cout << "Quel est le nom du client ? " << endl;
    string nom;
    cin>>nom;

    cout << "Quel est le prenom du client ? " << endl;
    string prenom;
    cin>>prenom;

    cli cli(nom,prenom);
    cli.insertCsv();

    cout << "ajout reussi ! " << endl;

}

int main()
{
    cout << " -- BIENVENUE -- " << endl;
    cout << " -- MENU PRINCIPAL -- " << endl;
    cout << " -- 1) Afficher la liste des vehicules vendus -- " << endl;
    cout << " -- 2) Afficher la liste des vehicules disponibles -- " << endl;
    cout << " -- 3) Afficher la liste de tous les vehicules -- " << endl;
    cout << " -- 4) Afficher la liste des clients -- " << endl;
    cout << " -- 5) Afficher la liste des vendeurs -- " << endl;
    cout << " -- 6) Ajout d'un vendeur -- " << endl;
    cout << " -- 7) Ajout un vehicule -- " << endl;
    cout << " -- 8) Ajout d'un client-- " << endl;
    cout << " -- 9) Vente d'un vehicule "<< endl;
    cout << " -- 10) Quitter  -- " << endl;

    cout << " Entrez le numero de votre choix :  " << endl;
    int nb;
    cin>>nb;

    /* choix du menu */
    switch (nb){

        case 1:
        cout<< "Liste des vehicules vendus" << endl;
        affichageVehiculesVendus();
        main();
        break;

        case 2:
        cout<< "Liste des vehicules disponibles"<< endl;
        AffichageVehDispo();
        main();
        break;

        case 3:
        cout<< "Liste des vehicules"<< endl;
        affichageVehicules();
        main();
        break;

        case 4:
        cout<< "Liste des clients"<< endl;
        affichageClients();
        main();
        break;

        case 5:
        cout<< "Liste des vendeurs"<< endl;
        affichageVendeurs();
        main();
        break;

        case 6:
        cout<< "Ajout d'un vendeur : "<< endl;
        AjoutVendeur();
        main();
        break;

        case 7:
        cout<< "Ajout d'un vehicule : "<< endl;
        AjoutVehicule();
        main();
        break;

        case 8:
        cout<< "Ajout d'un client : "<< endl;
        AjoutClient();
        main();
        break;

        case 9:
        cout<< "Ajout d'une vente d'un vehicule"<< endl;
        AjoutVente();
        main();
        break;

        case 10:
        exit(0);
        break;
    }
    exit(0);
    return 0;
}

