#include <iostream>

using namespace std;

struct vehOcc {
    int immId;
    string marque;
    string modele;
    string typEssence;
    string typTransmission;
    string couleur;
    int nbPortes;
    int prix;
};


int main()
{
    cout << "La voiture immatricul� "<< vehOcc.immId << " est un ou une "<< vehOcc.marque<< " mod�le "<< vehOcc.modele <<". Le type d'essence est du "<< vehOcc.typEssence<< " et la transmission "<< vehOcc.typTransmission<< " de couleur "<< vehOcc.couleur << " avec "<< vehOcc.nbPortes<< " portes et a un prix de " << vehOcc.prix << " Euros." << endl;
    return 0;
}
